# jpegresize
Simple tool to resize jpeg files in a folder.

## Usage
```
  -i string
        input path to a file or folder. (default ".")
  -q int
        compression quality. (default 100)
  -s float
        resize factor by which to multiply the size of the input image, for example .5 to divide the width and height both by 2. (default 0.5)
```
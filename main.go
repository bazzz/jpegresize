package main

import (
	"flag"
	"fmt"
	"image"
	"image/jpeg"
	"os"
	"path/filepath"

	"github.com/nfnt/resize"
)

var input string
var size float64
var quality int

func init() {
	flag.StringVar(&input, "i", ".", "input path to a file or folder.")
	flag.Float64Var(&size, "s", .5, "resize factor by which to multiply the size of the input image, for example .5 to divide the width and height both by 2.")
	flag.IntVar(&quality, "q", 100, "compression quality.")
	flag.Parse()
}

func main() {
	file, err := os.Open(input)
	if err != nil {
		fmt.Println("ERROR: input path does not exist:", err)
		os.Exit(1)
	}
	defer file.Close()
	stat, err := file.Stat()
	if err != nil {
		fmt.Println("ERROR: cannot open input path:", err)
		os.Exit(1)
	}
	jpegFiles := make([]string, 0)
	if stat.IsDir() {
		files, err := os.ReadDir(input)
		if err != nil {
			fmt.Println("ERROR: cannot open input path:", err)
			os.Exit(1)
		}
		for _, file := range files {
			if filepath.Ext(file.Name()) == ".jpg" || filepath.Ext(file.Name()) == ".jpeg" {
				jpegFiles = append(jpegFiles, filepath.Join(input, file.Name()))
			}
		}
	} else {
		jpegFiles = append(jpegFiles, input)
	}
	for _, jpegFile := range jpegFiles {
		fmt.Println("Processing", jpegFile)
		file, err := os.Open(jpegFile)
		if err != nil {
			fmt.Println("ERROR: cannot open", jpegFile, ":", err)
		}
		orgImage, _, err := image.Decode(file)
		if err != nil {
			fmt.Println("ERROR: cannot decode image", jpegFile, ":", err)
		}
		newSize := float64(orgImage.Bounds().Size().X) * size
		newImage := resize.Resize(uint(newSize), 0, orgImage, resize.Lanczos3)
		file.Close()
		file, err = os.Create(jpegFile)
		if err != nil {
			fmt.Println("ERROR: cannot create", jpegFile, ":", err)
		}
		if err = jpeg.Encode(file, newImage, &jpeg.Options{Quality: quality}); err != nil {
			fmt.Println("ERROR: cannot save new image:", err)
		}
		file.Close()
	}
}
